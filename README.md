# Speedtest - Graph Transformator

## Source Code

Can be found [here](https://bitbucket.org/account/user/mmprivat/projects/SPDTST)

## Description

This project was made to parse the logfile from my speedtest-logger and to create three JS / html charts from it.

The charts are a ping, upload and a download chart.  

## Prerequisites

* you need a logfile created by [speedtest-logger](https://bitbucket.org/mmprivat/speedtest-logger)
* you need a compiled version of this program 

## Compile

call 
`mvn clean package`
to compile this into a jar file which includes all dependencies.

## Run

### From source

To run this, you have to call the `Run` class (e.g. from you IDE)

### From JAR file

If you have a compiled version (also see Compile above) you can start this by calling (replace the two variables marked by $name)
`java -jar graph-transformator-0.1.1-SNAPSHOT-jar-with-dependencies.jar $YOUR_INPUT_FILE $YOUR_OUTPUT_FILE`

## License

This software project is licensed under the Apache Software Foundation license, version 2.0.

## Development

You need to get [ait-java-helpers](https://bitbucket.org/artindustrial-it/ait-java-helpers) in order to compile this project.
