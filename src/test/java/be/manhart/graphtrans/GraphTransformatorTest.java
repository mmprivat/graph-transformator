package be.manhart.graphtrans;

import java.sql.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class GraphTransformatorTest extends TestCase {
	/**
	 * Create the test case
	 *
	 * @param testName
	 *            name of the test case
	 */
	public GraphTransformatorTest(String testName) {
		super(testName);
	}

	/**
	 * @return the suite of tests being tested
	 */
	public static Test suite() {
		return new TestSuite(GraphTransformatorTest.class);
	}

//	public void testApp() {
//		Matcher m = GraphTransformator.LOG_ENTRY_DATE.matcher("Date: 1477345945");
//		System.out.println(m.matches());
//		System.out.println(m.groupCount());
//		System.out.println(m.group(1));
//	}
//
//	public void testDate() {
//		Matcher m = GraphTransformator.LOG_ENTRY_DATE.matcher("Date: 1477345945");
//		System.out.println(m.matches());
//		System.out.println(m.groupCount());
//		System.out.println(new Date(Long.parseLong(m.group(1)) * 1000));
//		
//	}
	
	public void testPing() {
//		Matcher m = GraphTransformator.LOG_ENTRY_PING.matcher("Hosted by Nessus GmbH (10G Uplink) (Vienna) [1.07 km]: 37.54 ms");
//		Matcher m = Pattern
//				.compile("Hosted by [A-Za-z0-9() ]+ \\[([0-9.]) km\\]: ([0-9.]) ms")
//				.matcher("Hosted by Nessus GmbH (10G Uplink) (Vienna) [1.07 km]: 37.54 ms");
		Matcher m = Pattern
				.compile("Hosted by [A-Za-z0-9() ]+ \\[([0-9.]+) km\\]: ([0-9.]+) ms")
				.matcher("Hosted by Nessus GmbH (10G Uplink) (Vienna) [1.07 km]: 37.54 ms");
		System.out.println(m.matches());
		System.out.println(m.groupCount());
		System.out.println(m.group(1));
		System.out.println(m.group(2));
	}
}
