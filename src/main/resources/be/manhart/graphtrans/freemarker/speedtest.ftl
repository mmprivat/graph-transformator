<!DOCTYPE html>
<html xmlns:wicket="http://wicket.apache.org">
	<head>
		<title>Speedtest Chart</title>
		<meta charset="utf-8" />
		<script src="flotr2.min.js" type="text/javascript"></script>
		<script src="charts-helpers.js" type="text/javascript"></script>
		<style>
			.chart {
				margin-bottom: 50px;
				min-height: 300px;
			}
		</style>
	</head>
	<body>
		<h2>Ping Chart</h2>
		<div id="ping-chart" class="chart"></div>
		<script type="text/javascript">
			${ping}
		</script>

		<h2>Upload Speed Chart</h2>
		<div id="upload-chart" class="chart"></div>
		<script type="text/javascript">
			${upload}
		</script>

		<h2>Download Speed Chart</h2>
		<div id="download-chart" class="chart"></div>
		<script type="text/javascript">
			${download}
		</script>
	</body>
</html>
