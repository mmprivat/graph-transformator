var monthNames = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ];

function getFormattedDate(timeStamp) {
	var string = float2int(timeStamp).toString();
	if (string.length == 2) {
		return "'" + string;
	} else if (string.length == 4) {
		return string;
	} else if (string.length == 6) {
		var year = string.substring(0, 4);
		var month = string.substring(4);
		return monthNames[month-1] + " " + year;
	} else if (string.length == 8) {
		var year = string.substring(0, 4);
		var month = string.substring(4, 6);
		var day = string.substring(6);
		return day + "." + month + "." + year;
	}
}

function getFormattedDateTime(timeStamp) {
	return new Date(parseInt(timeStamp)).format('DDD, DD.MM.YYYY HH:mm:ss');
}

function getFormattedTime(timeStamp) {
	var string = float2int(timeStamp).toString();
	if (string.length == 2) {
		return "'" + string;
	} else if (string.length == 4) {
		return string;
	} else if (string.length == 6) {
		var year = string.substring(0, 4);
		var month = string.substring(4);
		return monthNames[month-1] + " " + year;
	} else if (string.length == 8) {
		var year = string.substring(0, 4);
		var month = string.substring(4, 6);
		var day = string.substring(6);
		return day + "." + month + "." + year;
	}
}

function float2int (value) {
    return value | 0;
}


/*
js-date-format
v2.0
Author: Tony Brix (https://tony.brix.ninja)
https://github.com/UziTech/js-date-format
License: MIT
*/
!function(){Date.locales={en:{month_names:"January February March April May June July August September October November December".split(" "),month_names_short:"Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec".split(" "),day_names:"Sunday Monday Tuesday Wednesday Thursday Friday Saturday".split(" "),day_names_short:"Sun Mon Tue Wed Thu Fri Sat".split(" "),date_suffix:function(a){var b=a%10;return 1===~~(a%100/10)?"th":1===b?"st":2===b?"nd":3===b?"rd":"th"},meridiem:function(a,b,d){return 12>a?d?"am":
"AM":d?"pm":"PM"}}};Date.prototype.setLocale=function(a){a&&a in Date.locales&&(this.locale=a)};Date.setLocale=function(a){Date.prototype.setLocale(a)};Date.prototype.getLocale=function(){return this.locale||"en"};Date.getLocale=function(){Date.prototype.getLocale()};Date.prototype.getMonthName=function(a){var b="en";a&&a in Date.locales?b=a:this.locale&&this.locale in Date.locales&&(b=this.locale);return Date.locales[b].month_names[this.getMonth()]};Date.prototype.getMonthNameShort=function(a){var b=
"en";a&&a in Date.locales?b=a:this.locale&&this.locale in Date.locales&&(b=this.locale);return Date.locales[b].month_names_short[this.getMonth()]};Date.prototype.getDayName=function(a){var b="en";a&&a in Date.locales?b=a:this.locale&&this.locale in Date.locales&&(b=this.locale);return Date.locales[b].day_names[this.getDay()]};Date.prototype.getDayNameShort=function(a){var b="en";a&&a in Date.locales?b=a:this.locale&&this.locale in Date.locales&&(b=this.locale);return Date.locales[b].day_names_short[this.getDay()]};
Date.prototype.getDateSuffix=function(a){var b="en";a&&a in Date.locales?b=a:this.locale&&this.locale in Date.locales&&(b=this.locale);return Date.locales[b].date_suffix(this.getDate())};Date.prototype.getMeridiem=function(a,b){var d="en";b&&b in Date.locales?d=b:this.locale&&this.locale in Date.locales&&(d=this.locale);return Date.locales[d].meridiem(this.getHours(),this.getMinutes(),a)};Date.prototype.getLastDate=function(){return(new Date(this.getFullYear(),this.getMonth()+1,0)).getDate()};Date.prototype.format=
function(a){for(var b=function(a,b){for(var c="0",d=2;d<b;d++)c+="0";return(0>a?"-":"")+(c+Math.abs(a).toString()).slice(-b)},d={date:this,YYYY:function(){return this.date.getFullYear()},YY:function(){return this.date.getFullYear()%100},MMMM:function(){return this.date.getMonthName()},MMM:function(){return this.date.getMonthNameShort()},MM:function(){return b(this.date.getMonth()+1,2)},M:function(){return this.date.getMonth()+1},DDDD:function(){return this.date.getDayName()},DDD:function(){return this.date.getDayNameShort()},
DD:function(){return b(this.date.getDate(),2)},D:function(){return this.date.getDate()},S:function(){return this.date.getDateSuffix()},HH:function(){return b(this.date.getHours(),2)},H:function(){return this.date.getHours()},hh:function(){var a=this.date.getHours();12<a?a-=12:1>a&&(a=12);return b(a,2)},h:function(){var a=this.date.getHours();12<a?a-=12:1>a&&(a=12);return a},mm:function(){return b(this.date.getMinutes(),2)},m:function(){return this.date.getMinutes()},ss:function(){return b(this.date.getSeconds(),
2)},s:function(){return this.date.getSeconds()},fff:function(){return b(this.date.getMilliseconds(),3)},ff:function(){return b(Math.floor(this.date.getMilliseconds()/10),2)},f:function(){return Math.floor(this.date.getMilliseconds()/100)},zzzz:function(){return b(Math.floor(-this.date.getTimezoneOffset()/60),2)+":"+b(-this.date.getTimezoneOffset()%60,2)},zzz:function(){return Math.floor(-this.date.getTimezoneOffset()/60)+":"+b(-this.date.getTimezoneOffset()%60,2)},zz:function(){return b(Math.floor(-this.date.getTimezoneOffset()/
60),2)},z:function(){return Math.floor(-this.date.getTimezoneOffset()/60)},tt:function(){return this.date.getMeridiem(!0)},TT:function(){return this.date.getMeridiem(!1)}},e=[];0<a.length;)if('"'===a[0]){var c=/"[^"]*"/m.exec(a);null===c?(e.push(a.substring(1)),a=""):(c=c[0].substring(1,c[0].length-1),e.push(c),a=a.substring(c.length+2))}else if("'"===a[0])c=/'[^']*'/m.exec(a),null===c?(e.push(a.substring(1)),a=""):(c=c[0].substring(1,c[0].length-1),e.push(c),a=a.substring(c.length+2));else if("\\"===
a[0])1<a.length?(e.push(a.substring(1,2)),a=a.substring(2)):(e.push("\\"),a="");else{for(var c=!1,f=a.length;0<f;f--)if(a.substring(0,f)in d){e.push(d[a.substring(0,f)]());a=a.substring(f);c=!0;break}c||(e.push(a[0]),a=a.substring(1))}return e.join("")}}();