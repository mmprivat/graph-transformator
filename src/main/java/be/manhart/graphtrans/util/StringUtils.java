package be.manhart.graphtrans.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import be.manhart.graphtrans.util.Converter;
import be.manhart.graphtrans.util.StringConverter;

public class StringUtils {

	private static final String DEFAULT_SEPARATOR = ", ";

	public static int countOccurrences(String text, String charToFind) {
		if (charToFind == null || text == null) {
			return 0;
		}
		return (text.length() - text.replace(charToFind, "").length())
				/ charToFind.length();
	}

	public static String join(String separator, String... items) {
		if (items == null || items.length == 0) {
			return null;
		}
		return join(separator, Arrays.asList(items));
	}

	public static String join(String separator, List<String> items) {
		if (items == null || items.size() == 0) {
			return null;
		}
		return toString(items, new StringConverter<String>(), separator);
	}

	public static List<String> split(String string, String separator) {
		List<String> result = new ArrayList<String>();
		if (string != null) {
			result.addAll(Arrays.asList(string.split(separator)));
		}
		return result;
	}

	public static String toString(List<? extends Object> list) {
		return toString(list, new StringConverter());
	}

	public static <F> String toString(List<F> list, Converter<F, String> converter) {
		return toString(list, converter, DEFAULT_SEPARATOR);
	}

	public static <F> String toString(List<F> list, Converter<F, String> converter,
			String separator) {
		StringBuilder sb = new StringBuilder();
		if (list != null && list.size() > 0) {
			for (int i = 0; i < list.size(); i++) {
				sb.append(converter.convert(list.get(i)));
				if (i < list.size() - 1) {
					sb.append(separator);
				}
			}
		}
		return sb.toString();
	}

	public static String lowerFirstChar(String string) {
		return string.substring(0, 1).toLowerCase() + string.substring(1);
	}

	public static String upperFirstChar(String string) {
		return string.substring(0, 1).toUpperCase() + string.substring(1);
	}

	public static String generateUniqueId() {
		return new Date().getTime() + "" + UUID.randomUUID().toString();
	}

	public static boolean isNotNullOrEmpty(String string) {
		return !isNullOrEmpty(string);
	}

	public static boolean isNullOrEmpty(String text) {
		return text == null || text.trim().isEmpty();
	}

	public static String toString(Object object) {
		return new StringConverter().convert(object);
	}
}
