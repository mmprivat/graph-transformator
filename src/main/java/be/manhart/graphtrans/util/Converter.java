package be.manhart.graphtrans.util;

public interface Converter<F, T> {
	T convert(F object);
}