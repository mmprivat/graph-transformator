package be.manhart.graphtrans.util;

import java.lang.reflect.Method;
import java.util.Date;

import be.manhart.graphtrans.util.DateUtils;

public class StringConverter<F extends Object> implements Converter<F, String> {

	private Converter<F, ?> innerConverter;

	public StringConverter() {
	}

	public StringConverter(Converter<F, ?> innerConverter) {
		this.innerConverter = innerConverter;
	}

	public String convert(F object) {
		if (object != null) {
			Object o = object;
			if (innerConverter != null) {
				o = innerConverter.convert(object);
			}
			if (o instanceof String) {
				return (String) o;
			} else if (object instanceof Date) {
				return DateUtils.getFormattedDate((Date) o);
			} else {
				try {
					Method toStringMethod = o.getClass().getMethod("toString");
					if (toStringMethod.getDeclaringClass().equals(o.getClass())) {
						return o.toString();
					} else {
						Method[] methods = o.getClass().getDeclaredMethods();
						StringBuilder sb = new StringBuilder();
						sb.append(o.getClass().getSimpleName()).append("\n");
						for (Method method : methods) {
							if (!method.getReturnType().equals(Void.class)
									&& method.getParameterTypes().length == 0) {
								sb.append("    ").append(method.getName()).append(": ");
								try {
									sb.append(method.invoke(o)).append("\n");
								} catch (Exception e) {
									sb.append("[error getting value]").append("\n");
									e.printStackTrace();
								}
							}
						}
						return sb.toString();
					}
				} catch (NoSuchMethodException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (SecurityException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return null;
	}

}