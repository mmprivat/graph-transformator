package be.manhart.graphtrans.util;

public interface Filter<T> {

	public boolean isInFilter(T o);
}
