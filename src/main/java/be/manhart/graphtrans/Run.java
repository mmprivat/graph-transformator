package be.manhart.graphtrans;

import java.io.IOException;

import freemarker.template.TemplateException;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;

public class Run {
	private static final String TEMPLATE_ARG = "template";
	private static final String INFILE_ARG = "infile";
	private static final String OUTFILE_ARG = "outfile";

	public static void main(final String[] args) throws TemplateException, IOException {
		Config config = parseArguments(args);

		GraphTransformator g = new GraphTransformator(config);
		g.loadFile();
//		System.out.println("outpath: " + outputFile.getParent() + " - " + path 
//				+ " name: " + outputFile.getName());
		g.writeFile();
	}

	private static Config parseArguments(String[] args) throws IOException {
		ArgumentParser parser = ArgumentParsers.newArgumentParser("run")
                .description("Parses the logfile from \"speedtest-cli\" and creates a JS / html chart from it.");
        parser.addArgument(INFILE_ARG)
                .type(String.class)
                .help("fully qualified log file (e.g. /home/user/speedtest.log)");
        parser.addArgument(OUTFILE_ARG)
                .help("fully qualified output file (e.g. /home/user/speedtest.html)");
        parser.addArgument("-t")
		        .type(String.class)
		        .dest(TEMPLATE_ARG)
		        .help("fully qualified freemarker template (*.ftl) for the conversion into html");
        Config config = new Config();
        try {
            Namespace res = parser.parseArgs(args);
            config.setTemplateFile(res.getString(TEMPLATE_ARG));
            config.setInputFile(res.getString(INFILE_ARG));
            config.setOutputFile(res.getString(OUTFILE_ARG));
        } catch (ArgumentParserException e) {
            parser.handleError(e);
            System.exit(1);
        }
        return config;
	}
}
