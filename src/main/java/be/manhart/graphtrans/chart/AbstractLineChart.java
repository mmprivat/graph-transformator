package be.manhart.graphtrans.chart;

import be.manhart.graphtrans.chart.option.GridSettings;
import be.manhart.graphtrans.chart.option.LineChartSettings;
import be.manhart.graphtrans.chart.option.OptionSettings;
import be.manhart.graphtrans.chart.option.PointsSettings;
import be.manhart.graphtrans.chart.option.YaxisSettings;

/**
 * Das Line Chart nimmt ein {@link IModel}<{@link LineChartData}>.
 * 
 * @see BaseChart
 * @author Manuel
 * @param <E>
 * @param <F>
 */
public abstract class AbstractLineChart<T extends LineChartData<? extends Line<? extends Coordinates<E, F>>>, E, F>
		extends BaseChart<T> {
	private static final long serialVersionUID = 1L;

	public AbstractLineChart() {
		super();
	}

	public AbstractLineChart(final OptionSettings... settings) {
		super(settings);
	}

	@Override
	protected String convertDataToString(final T filteredData) {
		final StringBuilder sb = new StringBuilder();
		sb.append("[");
		if (filteredData.getLines().size() > 0) {
			// add all lines
			int lineCount = 0;
			for (final Line<? extends Coordinates<E, F>> line : filteredData.getLines()) {
				// add start
				if (lineCount > 0) {
					sb.append(",");
				}
				lineCount++;
				sb.append("{");
				// add all points
				sb.append("data: [");
				int coordCount = 0;
				if (line.getPoints() != null && line.getPoints().size() > 0) {
					for (final Coordinates<E, F> coord : line.getPoints()) {
						if (coordCount > 0) {
							sb.append(",");
						}
						sb.append("[" + convertX(coord.getX()) + "," + convertY(coord.getY()) + "]");
						coordCount++;
					}
				}
				sb.append("],");
				// add label
				sb.append("label: '");
				sb.append(line.getLabel());
				sb.append("'");
				if (line.isAlternateYaxis()) {
					sb.append(", yaxis: 2");
				}
				if (line.getColor() != null) {
					sb.append(", color: '" + line.getColor() + "'");
				}
				sb.append("}");
			}
		}
		sb.append("]");
		return sb.toString();
	}

	protected abstract String convertX(E x);

	protected abstract String convertY(F y);

	@Override
	protected OptionSettings[] getDefaultOptions() {
		return new OptionSettings[] { new LineChartSettings(), new PointsSettings(),
				new YaxisSettings().setShowLabels(true), new GridSettings().setMinorVerticalLines(false), };
	}
}
