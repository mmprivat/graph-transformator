package be.manhart.graphtrans.chart;

import java.util.Arrays;
import java.util.List;

public class TimelineChartData implements LineChartData<Timeline> {
	private static final long serialVersionUID = 1L;

	private List<Timeline> lines;

	public TimelineChartData(final Timeline... lines) {
		this.lines = Arrays.asList(lines);
	}
	
	public TimelineChartData(final List<Timeline> lines) {
		this.lines = lines;
	}

	@Override
	public List<Timeline> getLines() {
		return lines;
	}

	@Override
	public void setLines(final List<Timeline> lines) {
		this.lines = lines;
	}
}