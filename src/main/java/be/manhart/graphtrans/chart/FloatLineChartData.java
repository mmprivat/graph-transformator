package be.manhart.graphtrans.chart;

import java.util.ArrayList;
import java.util.List;

public class FloatLineChartData implements LineChartData<FloatLine> {
	private static final long serialVersionUID = 1L;

	private List<FloatLine> lines;

	public FloatLineChartData() {
		this(new ArrayList<>());
	}
	
	public FloatLineChartData(final List<FloatLine> lines) {
		setLines(lines);
	}

	@Override
	public List<FloatLine> getLines() {
		return lines;
	}

	@Override
	public void setLines(final List<FloatLine> lines) {
		this.lines = lines;
	}
}