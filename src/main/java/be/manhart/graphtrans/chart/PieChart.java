package be.manhart.graphtrans.chart;

import be.manhart.graphtrans.chart.option.GridSettings;
import be.manhart.graphtrans.chart.option.OptionSettings;
import be.manhart.graphtrans.chart.option.PieChartSettings;
import be.manhart.graphtrans.chart.option.XaxisSettings;
import be.manhart.graphtrans.chart.option.YaxisSettings;

/**
 * Das Pie Chart nimmt ein {@link IModel}<{@link PieChartData}>.
 * 
 * @see BaseChart
 * @author Manuel
 */
public class PieChart extends BaseChart<PieChartData> {
	private static final long serialVersionUID = 1L;

	public PieChart() {
		super();
	}

	public PieChart(OptionSettings... options) {
		super(options);
	}

	@Override
	protected String convertDataToString(PieChartData filteredData) {
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		if (filteredData.slices.length > 0) {
			// add all lines
			int lineCount = 0;
			for (Slice slice : filteredData.slices) {
				// add start
				if (lineCount > 0) {
					sb.append(",");
				}
				lineCount++;
				sb.append("{");
				// add all points
				sb.append("data: [");
				sb.append("[0," + slice.value + "]");
				sb.append("],");
				// add label
				sb.append("label: '");
				sb.append(slice.label);
				sb.append("'");
				sb.append("}");
			}
		}
		sb.append("]");
		return sb.toString();
	}

	@Override
	protected OptionSettings[] getDefaultOptions() {
		return new OptionSettings[] {
				new PieChartSettings().setExplode(6),
				new XaxisSettings().setShowLabels(false),
				new YaxisSettings().setShowLabels(false),
				new GridSettings().setVerticalLines(false).setHorizontalLines(false)
		};
	}
}
