package be.manhart.graphtrans.chart;

import java.util.List;

public class FloatLine implements Line<FloatCoordinates> {
	private static final long serialVersionUID = 1L;

	private List<FloatCoordinates> points;
	private boolean alternateYaxis;
	private String color;
	private String label;

	public FloatLine(final List<FloatCoordinates> points) {
		setPoints(points);
	}

	public FloatLine(final String label, final List<FloatCoordinates> points) {
		setLabel(label);
		setPoints(points);
	}

	public FloatLine(final List<FloatCoordinates> points, final boolean alternateYAxis) {
		this(points);
		setAlternateYaxis(alternateYAxis);
	}

	public FloatLine(final List<FloatCoordinates> points, final boolean alternateYAxis, final String color) {
		this(points, alternateYAxis);
		setColor(color);
	}

	@Override
	public List<FloatCoordinates> getPoints() {
		return points;
	}

	public void setPoints(final List<FloatCoordinates> points) {
		this.points = points;
	}

	@Override
	public boolean isAlternateYaxis() {
		return alternateYaxis;
	}

	public void setAlternateYaxis(final boolean alternateYaxis) {
		this.alternateYaxis = alternateYaxis;
	}

	@Override
	public String getColor() {
		return color;
	}

	@Override
	public void setColor(final String color) {
		this.color = color;
	}

	@Override
	public String getLabel() {
		return label;
	}

	@Override
	public void setLabel(String label) {
		this.label = label;
	}
}