package be.manhart.graphtrans.chart.option;

/**
 * wether to draw the text using HTML or on the canvas
 * 
 * @author Manuel
 * 
 */
public class HtmlTextSetting extends AbstractSettings {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final SettingKey<Boolean> HTML_TEXT = new SettingKey<Boolean>("HtmlText");

	public HtmlTextSetting() {
	}

	public HtmlTextSetting setHtmlText(final Boolean htmlText) {
		set(HtmlTextSetting.HTML_TEXT, htmlText);
		return this;
	}

	public Boolean getHtmlText() {
		return get(HtmlTextSetting.HTML_TEXT);
	}
}
