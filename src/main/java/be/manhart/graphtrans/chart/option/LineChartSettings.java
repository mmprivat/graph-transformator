package be.manhart.graphtrans.chart.option;

/**
 * @author Manuel
 */
public class LineChartSettings extends AbstractSettings {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final SettingKey<Integer> LINE_WIDTH = new SettingKey<Integer>("lineWidth");
	private static final SettingKey<Boolean> FILL = new SettingKey<Boolean>("fill");
	private static final SettingKey<String> FILL_COLOR = new SettingKey<String>("fillColor");
	private static final SettingKey<Boolean> FILL_BORDER = new SettingKey<Boolean>("fillBorder");
	private static final SettingKey<Boolean> STACKED = new SettingKey<Boolean>("stacked");
	private static final SettingKey<Boolean> STEPS = new SettingKey<Boolean>("steps");
	private static final SettingKey<Float> FILL_OPACITY = new SettingKey<Float>("fillOpacity");
	private static final SettingKey<Boolean> SHOW = new SettingKey<Boolean>("show");

	public LineChartSettings() {
		this(true);
	}

	public LineChartSettings(final Boolean isLineChart) {
		super();
		if (isLineChart != null) {
			set(LineChartSettings.SHOW, isLineChart);
		}
	}

	@Override
	public String getName() {
		return "lines";
	}

	public Integer getLineWidth() {
		return get(LineChartSettings.LINE_WIDTH);
	}

	/**
	 * @param lineWidth
	 *            the line width in pixels
	 */
	public LineChartSettings setLineWidth(final Integer lineWidth) {
		set(LineChartSettings.LINE_WIDTH, lineWidth);
		return this;
	}

	public Boolean getFill() {
		return get(LineChartSettings.FILL);
	}

	/**
	 * @param fill
	 *            true to fill the area from the line to the x axis, false for
	 *            (transparent) no fill
	 */
	public LineChartSettings setFill(final Boolean fill) {
		set(LineChartSettings.FILL, fill);
		return this;
	}

	public String getFillColor() {
		return get(LineChartSettings.FILL_COLOR);
	}

	/**
	 * @param fillColor
	 *            fill color
	 */
	public LineChartSettings setFillColor(final String fillColor) {
		set(LineChartSettings.FILL_COLOR, fillColor);
		setFill(true);
		return this;
	}

	public Float getFillOpacity() {
		return get(LineChartSettings.FILL_OPACITY);
	}

	/**
	 * @param fillOpacity
	 *            opacity of the fill color, set to 1 for a solid fill, 0 hides
	 *            the fill
	 */
	public LineChartSettings setFillOpacity(final Float fillOpacity) {
		set(LineChartSettings.FILL_OPACITY, fillOpacity);
		setFill(true);
		return this;
	}

	public Boolean getFillBorder() {
		return get(LineChartSettings.FILL_BORDER);
	}

	/**
	 * @param fillBorder
	 *            fill border
	 */
	public LineChartSettings setFillBorder(final Boolean fillBorder) {
		set(LineChartSettings.FILL_BORDER, fillBorder);
		setFill(true);
		return this;
	}

	public Boolean getSteps() {
		return get(LineChartSettings.STEPS);
	}

	/**
	 * @param steps
	 *            draw steps
	 */
	public LineChartSettings setSteps(final Boolean steps) {
		set(LineChartSettings.STEPS, steps);
		return this;
	}

	public Boolean getStacked() {
		return get(LineChartSettings.STACKED);
	}

	/**
	 * @param stacked
	 *            true will show stacked lines, false will show normal lines
	 */
	public LineChartSettings setStacked(final Boolean stacked) {
		set(LineChartSettings.STACKED, stacked);
		return this;
	}
}
