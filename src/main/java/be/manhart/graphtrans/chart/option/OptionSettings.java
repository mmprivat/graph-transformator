package be.manhart.graphtrans.chart.option;

import java.io.Serializable;

public interface OptionSettings extends Serializable {
	public String toFlotrString();

	public boolean mergeSettings(OptionSettings item);

}