package be.manhart.graphtrans.chart.option;

/**
 * @author Manuel
 */
public class BarChartSettings extends AbstractSettings {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final SettingKey<Boolean> STACKED = new SettingKey<Boolean>("stacked");
	private static final SettingKey<Boolean> HORIZONTAL = new SettingKey<Boolean>("horizontal");
	private static final SettingKey<Integer> LINE_WIDTH = new SettingKey<Integer>("lineWidth");
	private static final SettingKey<Float> BAR_WIDTH = new SettingKey<Float>("barWidth");
	private static final SettingKey<Boolean> SHOW = new SettingKey<Boolean>("show");

	public BarChartSettings() {
		this(true);
	}

	public BarChartSettings(final Boolean isBarChart) {
		super();
		if (isBarChart != null) {
			set(BarChartSettings.SHOW, isBarChart);
		}
	}

	@Override
	public String getName() {
		return "bars";
	}

	public Integer getLineWidth() {
		return get(BarChartSettings.LINE_WIDTH);
	}

	/**
	 * @param lineWidth
	 *            the line width in pixels
	 */
	public BarChartSettings setLineWidth(final Integer lineWidth) {
		set(BarChartSettings.LINE_WIDTH, lineWidth);
		return this;
	}

	public Boolean getHorizontal() {
		return get(BarChartSettings.HORIZONTAL);
	}

	/**
	 * @param horizontal
	 *            true to display horizontal bars, false for vertical bars
	 */
	public BarChartSettings setHorizontal(final Boolean horizontal) {
		set(BarChartSettings.HORIZONTAL, horizontal);
		return this;
	}

	public Float getBarWidth() {
		return get(BarChartSettings.BAR_WIDTH);
	}

	/**
	 * @param fillOpacity
	 *            opacity of the fill color, set to 1 for a solid fill, 0 hides
	 *            the fill
	 */
	public BarChartSettings setBarWidth(final Float barWidth) {
		set(BarChartSettings.BAR_WIDTH, barWidth);
		return this;
	}

	public Boolean getStacked() {
		return get(BarChartSettings.STACKED);
	}

	/**
	 * @param stacked
	 *            true will show stacked lines, false will show normal lines
	 */
	public BarChartSettings setStacked(final Boolean stacked) {
		set(BarChartSettings.STACKED, stacked);
		return this;
	}
}
