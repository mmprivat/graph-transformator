package be.manhart.graphtrans.chart.option;

/**
 * @author Manuel
 */
public class X2AxisSettings extends AbstractAxisSettings {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public X2AxisSettings() {
	}

	@Override
	public String getName() {
		return "x2axis";
	}

	private static final SettingKey<Mode> MODE = new SettingKey<Mode>("mode");
	private static final SettingKey<String> TIME_FORMAT = new SettingKey<String>("timeFormat");
	private static final SettingKey<TimeMode> TIME_MODE = new SettingKey<TimeMode>("timeMode");
	private static final SettingKey<TimeUnit> TIME_UNIT = new SettingKey<TimeUnit>("timeUnit");

	public Mode getMode() {
		return get(X2AxisSettings.MODE);
	}

	/**
	 * @param mode
	 *            can be 'time' or 'normal'
	 */
	public X2AxisSettings setMode(final Mode mode) {
		set(X2AxisSettings.MODE, mode);
		return this;
	}

	public String getTimeFormat() {
		return get(X2AxisSettings.TIME_FORMAT);
	}

	/**
	 * @param timeFormat
	 */
	public X2AxisSettings setTimeFormat(final String timeFormat) {
		set(X2AxisSettings.TIME_FORMAT, timeFormat);
		if (timeFormat != null) {
			set(X2AxisSettings.MODE, Mode.TIME);
		}
		return this;
	}

	public TimeMode getTimeMode() {
		return get(X2AxisSettings.TIME_MODE);
	}

	/**
	 * @param timeMode
	 *            'UTC' for UTC time, 'local' for local time
	 */
	public X2AxisSettings setTimeMode(final TimeMode timeMode) {
		set(X2AxisSettings.TIME_MODE, timeMode);
		if (timeMode != null) {
			set(X2AxisSettings.MODE, Mode.TIME);
		}
		return this;
	}

	public TimeUnit getTimeUnit() {
		return get(X2AxisSettings.TIME_UNIT);
	}

	/**
	 * @param timeUnit
	 *            Unit for time (millisecond, second, minute, hour, day, month,
	 *            year)
	 */
	public X2AxisSettings setTimeUnit(final TimeUnit timeUnit) {
		set(X2AxisSettings.TIME_UNIT, timeUnit);
		if (timeUnit != null) {
			set(X2AxisSettings.MODE, Mode.TIME);
			switch (timeUnit) {
			case MILLISECOND:
			case MINUTE:
			case HOUR:
				// TODO MM move the 2 default JS function here?
				// function(x) {
				// var
				// x = parseInt(x),
				// months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul',
				// 'Aug', 'Sep',
				// 'Oct', 'Nov', 'Dec'];
				// return months[(x - 1) % 12];
				// }

				// setTickFormatter("getFormattedTime");
				break;
			case DAY:
			case MONTH:
			case YEAR:
			default:
				// setTickFormatter("getFormattedDate");
				break;
			}
		}
		return this;
	}

	@Override
	protected String convertSettingValueToFlotrString(final SettingKey<?> key, Object value) {
		if (key == X2AxisSettings.MODE || key == X2AxisSettings.TIME_MODE || key == X2AxisSettings.TIME_UNIT) {
			value = value.toString();
		}
		final String result = super.convertSettingValueToFlotrString(key, value);
		return result;
	}

	public static enum Mode {
		NORMAL, TIME;
		@Override
		public String toString() {
			return name().toLowerCase();
		};
	}

	public static enum TimeMode {
		UTC, LOCAL;
		@Override
		public String toString() {
			if (this == UTC) {
				return name();
			} else {
				return name().toLowerCase();
			}
		};
	}

	public static enum TimeUnit {
		MILLISECOND, SECOND, MINUTE, HOUR, DAY, MONTH, YEAR;
		@Override
		public String toString() {
			return name().toLowerCase();
		};
	}
}
