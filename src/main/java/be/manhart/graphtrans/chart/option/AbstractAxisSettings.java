package be.manhart.graphtrans.chart.option;

/**
 * Defines the maximum value to show.<br/>
 * Useful for {@link XAxisOption} and {@link YAxisOption}.
 * 
 * @author Manuel
 */
public abstract class AbstractAxisSettings extends AbstractSettings {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AbstractAxisSettings() {
	}

	@Override
	public abstract String getName();

	private static final SettingKey<String> TICKS = new SettingKey<String>("ticks", true);
	private static final SettingKey<String> MINOR_TICKS = new SettingKey<String>("minorTicks");
	private static final SettingKey<Boolean> SHOW_LABELS = new SettingKey<Boolean>("showLabels");
	private static final SettingKey<Boolean> SHOW_MINOR_LABELS = new SettingKey<Boolean>("showMinorLabels");
	private static final SettingKey<Integer> LABELS_ANGLE = new SettingKey<Integer>("labelsAngle");
	private static final SettingKey<String> TITLE = new SettingKey<String>("title");
	private static final SettingKey<Integer> TITLE_ANGLE = new SettingKey<Integer>("titleAngle");
	private static final SettingKey<Integer> NO_TICKS = new SettingKey<Integer>("noTicks");
	private static final SettingKey<Integer> MINOR_TICK_FREQ = new SettingKey<Integer>("minorTickFreq");
	private static final SettingKey<String> TICK_FORMATTER = new SettingKey<String>("tickFormatter", true);
	private static final SettingKey<Integer> TICK_DECIMALS = new SettingKey<Integer>("tickDecimals");
	private static final SettingKey<Long> MIN = new SettingKey<Long>("min");
	private static final SettingKey<Long> MAX = new SettingKey<Long>("max");
	private static final SettingKey<Boolean> AUTOSCALE = new SettingKey<Boolean>("autoscale");
	private static final SettingKey<Integer> AUTOSCALE_MARGIN = new SettingKey<Integer>("autoscaleMargin");
	private static final SettingKey<String> COLOR = new SettingKey<String>("color");
	private static final SettingKey<Scaling> SCALING = new SettingKey<Scaling>("scaling");
	private static final SettingKey<String> BASE = new SettingKey<String>("base");
	private static final SettingKey<Align> TITLE_ALIGN = new SettingKey<Align>("titleAlign");
	private static final SettingKey<Boolean> MARGIN = new SettingKey<Boolean>("margin");

	public String getTicks() {
		return get(AbstractAxisSettings.TICKS);
	}

	/**
	 * @param ticks
	 *            format; either [1, 3] or [[1, 'a'], 3]
	 * @return
	 */
	public AbstractAxisSettings setTicks(final String ticks) {
		set(AbstractAxisSettings.TICKS, ticks);
		return this;
	}

	public String getMinorTicks() {
		return get(AbstractAxisSettings.MINOR_TICKS);
	}

	/**
	 * @param minorTicks
	 *            format; either [1, 3] or [[1, 'a'], 3]
	 * @return
	 */
	public AbstractAxisSettings setMinorTicks(final String minorTicks) {
		set(AbstractAxisSettings.MINOR_TICKS, minorTicks);
		return this;
	}

	public Boolean getShowLabels() {
		return get(AbstractAxisSettings.SHOW_LABELS);
	}

	/**
	 * @param showLabels
	 *            setting to true will show the axis ticks labels, hide
	 *            otherwise
	 * @return
	 */
	public AbstractAxisSettings setShowLabels(final Boolean showLabels) {
		set(AbstractAxisSettings.SHOW_LABELS, showLabels);
		return this;
	}

	public Boolean getShowMinorLabels() {
		return get(AbstractAxisSettings.SHOW_MINOR_LABELS);
	}

	/**
	 * @param showMinorLabels
	 *            true to show the axis minor ticks labels, false to hide
	 * @return
	 */
	public AbstractAxisSettings setShowMinorLabels(final Boolean showMinorLabels) {
		set(AbstractAxisSettings.SHOW_MINOR_LABELS, showMinorLabels);
		return this;
	}

	public Integer getLabelsAngle() {
		return get(AbstractAxisSettings.LABELS_ANGLE);
	}

	/**
	 * @param labelsAngle
	 *            labels' angle, in degrees
	 */
	public AbstractAxisSettings setLabelsAngle(final Integer labelsAngle) {
		set(AbstractAxisSettings.LABELS_ANGLE, labelsAngle);
		return this;
	}

	public String getTitle() {
		return get(AbstractAxisSettings.TITLE);
	}

	/**
	 * @param title
	 *            axis title
	 * @return
	 */
	public AbstractAxisSettings setTitle(final String title) {
		set(AbstractAxisSettings.TITLE, title);
		return this;
	}

	public Integer getTitleAngle() {
		return get(AbstractAxisSettings.TITLE_ANGLE);
	}

	/**
	 * @param titleAngle
	 *            axis title's angle, in degrees
	 */
	public AbstractAxisSettings setTitleAngle(final Integer titleAngle) {
		set(AbstractAxisSettings.TITLE_ANGLE, titleAngle);
		return this;
	}

	public Integer getNoTicks() {
		return get(AbstractAxisSettings.NO_TICKS);
	}

	/**
	 * @param noTicks
	 *            number of ticks for automagically generated ticks
	 */
	public AbstractAxisSettings setNoTicks(final Integer noTicks) {
		set(AbstractAxisSettings.NO_TICKS, noTicks);
		return this;
	}

	public Integer getMinorTickFreq() {
		return get(AbstractAxisSettings.MINOR_TICK_FREQ);
	}

	/**
	 * @param minorTickFreq
	 *            number of minor ticks between major ticks for autogenerated
	 *            ticks
	 */
	public AbstractAxisSettings setMinorTickFreq(final Integer minorTickFreq) {
		set(AbstractAxisSettings.MINOR_TICK_FREQ, minorTickFreq);
		return this;
	}

	public String getTickFormatter() {
		return get(AbstractAxisSettings.TICK_FORMATTER);
	}

	/**
	 * @param tickFormatter
	 *            Flotr.defaultTickFormatter, => fn; number, Object -> string
	 */
	public AbstractAxisSettings setTickFormatter(final String tickFormatter) {
		set(AbstractAxisSettings.TICK_FORMATTER, tickFormatter);
		return this;
	}

	public Integer getTickDecimals() {
		return get(AbstractAxisSettings.TICK_DECIMALS);
	}

	/**
	 * @param tickDecimals
	 *            => no. of decimals, null means auto
	 */
	public AbstractAxisSettings setTickDecimals(final Integer tickDecimals) {
		set(AbstractAxisSettings.TICK_DECIMALS, tickDecimals);
		return this;
	}

	public Long getMin() {
		return get(AbstractAxisSettings.MIN);
	}

	/**
	 * @param min
	 *            min. value to show, null means set automatically
	 */
	public AbstractAxisSettings setMin(final Long min) {
		set(AbstractAxisSettings.MIN, min);
		return this;
	}

	public Long getMax() {
		return get(AbstractAxisSettings.MAX);
	}

	/**
	 * @param max
	 *            max. value to show, null means set // automatically
	 */
	public AbstractAxisSettings setMax(final Long max) {
		set(AbstractAxisSettings.MAX, max);
		return this;
	}

	public Boolean getAutoscale() {
		return get(AbstractAxisSettings.AUTOSCALE);
	}

	/**
	 * @param autoscale
	 *            turns autoscaling on with true
	 */
	public AbstractAxisSettings setAutoscale(final Boolean autoscale) {
		set(AbstractAxisSettings.AUTOSCALE, autoscale);
		return this;
	}

	public Integer getAutoscaleMargin() {
		return get(AbstractAxisSettings.AUTOSCALE_MARGIN);
	}

	/**
	 * @param autoscaleMargin
	 *            margin in % to add if auto-setting min/max
	 */
	public AbstractAxisSettings setAutoscaleMargin(final Integer autoscaleMargin) {
		set(AbstractAxisSettings.AUTOSCALE_MARGIN, autoscaleMargin);
		return this;
	}

	public String getColor() {
		return get(AbstractAxisSettings.COLOR);
	}

	/**
	 * @param color
	 *            color of the ticks
	 */
	public AbstractAxisSettings setColor(final String color) {
		set(AbstractAxisSettings.COLOR, color);
		return this;
	}

	public Scaling getScaling() {
		return get(AbstractAxisSettings.SCALING);
	}

	/**
	 * @param scaling
	 *            scaling, can be linear or logarithmic
	 */
	public AbstractAxisSettings setScaling(final Scaling scaling) {
		set(AbstractAxisSettings.SCALING, scaling);
		return this;
	}

	public String getBase() {
		return get(AbstractAxisSettings.BASE);
	}

	/**
	 * @param base
	 *            Math.E
	 */
	public AbstractAxisSettings setBase(final String base) {
		set(AbstractAxisSettings.BASE, base);
		return this;
	}

	public Align getTitleAlign() {
		return get(AbstractAxisSettings.TITLE_ALIGN);
	}

	/**
	 * @param titleAlign
	 *            left, right, center
	 */
	public AbstractAxisSettings setTitleAlign(final Align titleAlign) {
		set(AbstractAxisSettings.TITLE_ALIGN, titleAlign);
		return this;
	}

	public Boolean getMargin() {
		return get(AbstractAxisSettings.MARGIN);
	}

	/**
	 * @param margin
	 *            turn off margins with false
	 */
	public AbstractAxisSettings setMargin(final Boolean margin) {
		set(AbstractAxisSettings.MARGIN, margin);
		return this;
	}

	public static enum Scaling {
		LINEAR, LOGARITHMIC;
	}

	public static enum Align {
		LEFT, CENTER, RIGHT;
	}
}
