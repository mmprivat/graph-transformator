package be.manhart.graphtrans.chart.option;

public class PointsSettings extends AbstractSettings {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PointsSettings() {
		this(true);
	}

	public PointsSettings(final Boolean show) {
		super();
		if (show != null) {
			setShow(show);
		}
	}

	@Override
	public String getName() {
		return "points";
	}

	public static final SettingKey<Boolean> SHOW = new SettingKey<Boolean>("show");

	public PointsSettings setShow(final Boolean show) {
		set(PointsSettings.SHOW, show);
		return this;
	}

	public Boolean getShow() {
		return get(PointsSettings.SHOW);
	}
}
