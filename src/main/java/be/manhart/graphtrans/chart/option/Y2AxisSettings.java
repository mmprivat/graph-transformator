package be.manhart.graphtrans.chart.option;

/**
 * @author Manuel
 */
public class Y2AxisSettings extends AbstractAxisSettings {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Y2AxisSettings() {
	}

	@Override
	public String getName() {
		return "y2axis";
	}
}
