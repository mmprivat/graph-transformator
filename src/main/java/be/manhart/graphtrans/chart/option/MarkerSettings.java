package be.manhart.graphtrans.chart.option;

public class MarkerSettings extends AbstractSettings {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final SettingKey<Boolean> SHOW = new SettingKey<Boolean>("show");
	private static final SettingKey<String> POSITION = new SettingKey<String>("position");

	public MarkerSettings() {
		this(true);
	}

	public MarkerSettings(final Boolean show) {
		if (show != null) {
			setShow(show);
		}
	}

	@Override
	public String getName() {
		return "markers";
	}

	public String getPosition() {
		return get(MarkerSettings.POSITION);
	}

	/**
	 * @param position
	 *            e.g. nw, n, se,...
	 */
	public MarkerSettings setPosition(final String position) {
		set(MarkerSettings.POSITION, position);
		return this;
	}

	public Boolean getShow() {
		return get(MarkerSettings.SHOW);
	}

	public MarkerSettings setShow(final Boolean show) {
		set(MarkerSettings.SHOW, show);
		return this;
	}
}