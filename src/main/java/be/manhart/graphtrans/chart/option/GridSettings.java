package be.manhart.graphtrans.chart.option;

public class GridSettings extends AbstractSettings {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final SettingKey<String> COLOR = new SettingKey<String>("color");
	private final SettingKey<String> BACKGROUND_COLOR = new SettingKey<String>("backgroundColor") {
		/**
				 * 
				 */
		private static final long serialVersionUID = 1L;

		@Override
		public boolean isFunction() {
			final String value = getBackgroundColor();
			return value.toString().startsWith("{") && value.toString().endsWith("}");
		};
	};
	private static final SettingKey<String> BACKGROUND_IMAGE = new SettingKey<String>("backgroundImage");
	private static final SettingKey<Boolean> VERTICAL_LINES = new SettingKey<Boolean>("verticalLines");
	private static final SettingKey<Boolean> MINOR_VERTICAL_LINES = new SettingKey<Boolean>("minorVerticalLines");
	private static final SettingKey<Boolean> HORIZONTAL_LINES = new SettingKey<Boolean>("horizontalLines");
	private static final SettingKey<Boolean> MINOR_HORIZONTAL_LINES = new SettingKey<Boolean>("minorHorizontalLines");
	private static final SettingKey<Float> WATERMARK_ALPHA = new SettingKey<Float>("watermarkAlpha");
	private static final SettingKey<String> TICK_COLOR = new SettingKey<String>("tickColor");
	private static final SettingKey<Integer> LABEL_MARGIN = new SettingKey<Integer>("labelMargin");
	private static final SettingKey<Integer> OUTLINE_WIDTH = new SettingKey<Integer>("outlineWidth");
	private static final SettingKey<String> OUTLINE = new SettingKey<String>("outline");
	private static final SettingKey<Boolean> CIRCULAR = new SettingKey<Boolean>("circular");
	boolean isBackgroundFunction;

	public GridSettings() {
	}

	@Override
	public String getName() {
		return "grid";
	}

	public String getColor() {
		return super.get(GridSettings.COLOR);
	}

	/**
	 * @param color
	 *            primary color used for outline and labels, null for default
	 *            value
	 * @return
	 */
	public GridSettings setColor(final String color) {
		super.set(GridSettings.COLOR, color);
		return this;
	}

	public String getBackgroundColor() {
		return super.get(BACKGROUND_COLOR);
	}

	/**
	 * You can give a background color here, or a function in
	 * {@link GridSettings#setBackgroundFunction(String)}.
	 * 
	 * @param backgroundColor
	 *            null for transparent, else color
	 * @return
	 */
	public GridSettings setBackgroundColor(final String backgroundColor) {
		super.set(BACKGROUND_COLOR, backgroundColor);
		isBackgroundFunction = false;
		return this;
	}

	/**
	 * Sets a function as background color, like
	 * 
	 * <pre>
	 * {@code
	 * colors: [[0, '#fff'], [1, '#eee']], 
	 * start: 'top', 
	 * end: 'bottom'
	 * }
	 * </pre>
	 * 
	 * for a gradient from light gray to white.
	 * 
	 * @param backgroundFunction
	 * @return
	 */
	public GridSettings setBackgroundFunction(final String backgroundFunction) {
		super.set(BACKGROUND_COLOR, backgroundFunction);
		isBackgroundFunction = true;
		return this;
	}

	public String getBackgroundImage() {
		return super.get(GridSettings.BACKGROUND_IMAGE);
	}

	/**
	 * @param backgroundImage
	 *            background image src
	 * @return
	 */
	public GridSettings setBackgroundImage(final String backgroundImage) {
		super.set(GridSettings.BACKGROUND_IMAGE, backgroundImage);
		return this;
	}

	public Boolean getVerticalLines() {
		return super.get(GridSettings.VERTICAL_LINES);
	}

	/**
	 * @param verticalLines
	 *            whether to show gridlines in vertical direction
	 * @return
	 */
	public GridSettings setVerticalLines(final Boolean verticalLines) {
		super.set(GridSettings.VERTICAL_LINES, verticalLines);
		return this;
	}

	public Boolean getHorizontalLines() {
		return super.get(GridSettings.HORIZONTAL_LINES);
	}

	/**
	 * @param horizontalLines
	 *            whether to show gridlines in horizontal direction
	 * @return
	 */
	public GridSettings setHorizontalLines(final Boolean horizontalLines) {
		super.set(GridSettings.HORIZONTAL_LINES, horizontalLines);
		return this;
	}

	public String getTickColor() {
		return super.get(GridSettings.TICK_COLOR);
	}

	/**
	 * @param tickColor
	 *            color used for the ticks
	 * @return
	 */
	public GridSettings setTickColor(final String tickColor) {
		super.set(GridSettings.TICK_COLOR, tickColor);
		return this;
	}

	public Integer getLabelMargin() {
		return super.get(GridSettings.LABEL_MARGIN);
	}

	/**
	 * @param labelMargin
	 *            margin in pixels
	 */
	public GridSettings setLabelMargin(final Integer labelMargin) {
		super.set(GridSettings.LABEL_MARGIN, labelMargin);
		return this;
	}

	public Float getWatermarkAlpha() {
		return super.get(GridSettings.WATERMARK_ALPHA);
	}

	/**
	 * @param watermarkAlpha
	 */
	public GridSettings setWatermarkAlpha(final Float watermarkAlpha) {
		super.set(GridSettings.WATERMARK_ALPHA, watermarkAlpha);
		return this;
	}

	public Integer getOutlineWidth() {
		return super.get(GridSettings.OUTLINE_WIDTH);
	}

	/**
	 * @param outlineWidth
	 *            width of the grid outline/border in pixels
	 */
	public GridSettings setOutlineWidth(final Integer outlineWidth) {
		super.set(GridSettings.OUTLINE_WIDTH, outlineWidth);
		return this;
	}

	public String getOutline() {
		return super.get(GridSettings.OUTLINE);
	}

	/**
	 * @param outline
	 *            walls of the outline to display, e.g. nsew
	 */
	public GridSettings setOutline(final String outline) {
		super.set(GridSettings.OUTLINE, outline);
		return this;
	}

	public Boolean getCircular() {
		return super.get(GridSettings.CIRCULAR);
	}

	/**
	 * @param circular
	 *            if set to true, the grid will be circular, must be used when
	 *            radars are drawn
	 */
	public GridSettings setCircular(final Boolean circular) {
		super.set(GridSettings.CIRCULAR, circular);
		return this;
	}

	public Boolean getMinorVerticalLines() {
		return super.get(GridSettings.MINOR_VERTICAL_LINES);
	}

	/**
	 * @param minorVerticalLines
	 *            whether to show gridlines for minor ticks in vertical
	 *            direction
	 */
	public GridSettings setMinorVerticalLines(final Boolean minorVerticalLines) {
		super.set(GridSettings.MINOR_VERTICAL_LINES, minorVerticalLines);
		return this;
	}

	public Boolean getMinorHorizontalLines() {
		return super.get(GridSettings.MINOR_HORIZONTAL_LINES);
	}

	/**
	 * @param minorHorizontalLines
	 *            whether to show gridlines for minor ticks in horizontal
	 *            direction
	 */
	public GridSettings setMinorHorizontalLines(final Boolean minorHorizontalLines) {
		super.set(GridSettings.MINOR_HORIZONTAL_LINES, minorHorizontalLines);
		return this;
	}
}
