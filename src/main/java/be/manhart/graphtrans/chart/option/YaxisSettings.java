package be.manhart.graphtrans.chart.option;

/**
 * @author Manuel
 */
public class YaxisSettings extends AbstractAxisSettings {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public YaxisSettings() {
	}

	@Override
	public String getName() {
		return "yaxis";
	}
}
