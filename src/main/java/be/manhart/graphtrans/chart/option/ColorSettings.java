package be.manhart.graphtrans.chart.option;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ColorSettings extends AbstractSettings {
	/**
	* 
	*/
	private static final long serialVersionUID = 1L;
	private static final SettingKey<List<String>> COLORS = new SettingKey<List<String>>("colors");

	public ColorSettings() {
	}

	// public String toFlotrString() {
	// if (colors == null || colors.size() == 0) {
	// return null;
	// } else {
	// StringBuilder result = new StringBuilder();
	// result.append("colors: [");
	// for (String item : colors) {
	// result.append("'");
	// result.append(item);
	// result.append("'");
	// result.append(",");
	// }
	// // replace last ',' with ']'
	// int start = result.length() - 2;
	// int end = start + 1;
	// return result.replace(start, end, "],").toString();
	// }
	// }

	public void clear() {
		remove(ColorSettings.COLORS);
	}

	public ColorSettings add(final String... colors) {
		List<String> list = get(ColorSettings.COLORS);
		if (list == null) {
			list = new ArrayList<String>();
		}
		list.addAll(Arrays.asList(colors));
		set(list);
		return this;
	}

	public ColorSettings set(final List<String> colors) {
		set(ColorSettings.COLORS, colors);
		return this;
	}
}