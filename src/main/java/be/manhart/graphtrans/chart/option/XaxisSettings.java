package be.manhart.graphtrans.chart.option;

/**
 * @author Manuel
 */
public class XaxisSettings extends AbstractAxisSettings {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public XaxisSettings() {
	}

	@Override
	public String getName() {
		return "xaxis";
	}

	private static final SettingKey<Mode> MODE = new SettingKey<Mode>("mode");
	private static final SettingKey<String> TIME_FORMAT = new SettingKey<String>("timeFormat");
	private static final SettingKey<TimeMode> TIME_MODE = new SettingKey<TimeMode>("timeMode");
	private static final SettingKey<TimeUnit> TIME_UNIT = new SettingKey<TimeUnit>("timeUnit");

	public Mode getMode() {
		return get(XaxisSettings.MODE);
	}

	/**
	 * @param mode
	 *            can be 'time' or 'normal'
	 */
	public XaxisSettings setMode(final Mode mode) {
		set(XaxisSettings.MODE, mode);
		return this;
	}

	public String getTimeFormat() {
		return get(XaxisSettings.TIME_FORMAT);
	}

	/**
	 * @param timeFormat
	 */
	public XaxisSettings setTimeFormat(final String timeFormat) {
		set(XaxisSettings.TIME_FORMAT, timeFormat);
		if (timeFormat != null) {
			set(XaxisSettings.MODE, Mode.TIME);
		}
		return this;
	}

	public TimeMode getTimeMode() {
		return get(XaxisSettings.TIME_MODE);
	}

	/**
	 * @param timeMode
	 *            'UTC' for UTC time, 'local' for local time
	 */
	public XaxisSettings setTimeMode(final TimeMode timeMode) {
		set(XaxisSettings.TIME_MODE, timeMode);
		if (timeMode != null) {
			set(XaxisSettings.MODE, Mode.TIME);
		}
		return this;
	}

	public TimeUnit getTimeUnit() {
		return get(XaxisSettings.TIME_UNIT);
	}

	/**
	 * @param timeUnit
	 *            Unit for time (millisecond, second, minute, hour, day, month,
	 *            year)
	 */
	public XaxisSettings setTimeUnit(final TimeUnit timeUnit) {
		set(XaxisSettings.TIME_UNIT, timeUnit);
		if (timeUnit != null) {
			set(XaxisSettings.MODE, Mode.TIME);
			switch (timeUnit) {
			case MILLISECOND:
			case MINUTE:
			case HOUR:
				// TODO MM move the 2 default JS function here?
				// function(x) {
				// var
				// x = parseInt(x),
				// months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul',
				// 'Aug', 'Sep',
				// 'Oct', 'Nov', 'Dec'];
				// return months[(x - 1) % 12];
				// }

				// setTickFormatter("getFormattedTime");
				break;
			case DAY:
			case MONTH:
			case YEAR:
			default:
				// setTickFormatter("getFormattedDate");
				break;
			}
		}
		return this;
	}

	@Override
	protected String convertSettingValueToFlotrString(final SettingKey<?> key, Object value) {
		if (key == XaxisSettings.MODE || key == XaxisSettings.TIME_MODE || key == XaxisSettings.TIME_UNIT) {
			value = value.toString();
		}
		final String result = super.convertSettingValueToFlotrString(key, value);
		return result;
	}

	public static enum Mode {
		NORMAL, TIME;
		@Override
		public String toString() {
			return name().toLowerCase();
		};
	}

	public static enum TimeMode {
		UTC, LOCAL;
		@Override
		public String toString() {
			if (this == UTC) {
				return name();
			} else {
				return name().toLowerCase();
			}
		};
	}

	public static enum TimeUnit {
		MILLISECOND, SECOND, MINUTE, HOUR, DAY, MONTH, YEAR;
		@Override
		public String toString() {
			return name().toLowerCase();
		};
	}
}
