package be.manhart.graphtrans.chart;

import java.util.List;

public class Timeline implements Line<TimeCoordinates> {
	private static final long serialVersionUID = 1L;
	
	private List<TimeCoordinates> points;
	private boolean alternateYaxis;
	private String color;
	private String label;

	public Timeline(List<TimeCoordinates> points) {
		setPoints(points);
	}

	public Timeline(List<TimeCoordinates> points, boolean alternateYaxis) {
		this(points);
		setAlternateYaxis(alternateYaxis);
	}
	
	public Timeline(List<TimeCoordinates> points, String color, boolean alternateYaxis) {
		this(points, alternateYaxis);
		setColor(color);
	}
	
	public Timeline(String label, List<TimeCoordinates> timeCoordinates) {
		setLabel(label);
		setPoints(timeCoordinates);
	}

	public List<TimeCoordinates> getPoints() {
		return points;
	}

	public void setPoints(List<TimeCoordinates> points) {
		this.points = points;
	}

	public boolean isAlternateYaxis() {
		return alternateYaxis;
	}

	public void setAlternateYaxis(boolean alternateYaxis) {
		this.alternateYaxis = alternateYaxis;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
}