package be.manhart.graphtrans.chart;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import be.manhart.graphtrans.util.DateUtils;

import be.manhart.graphtrans.chart.option.OptionSettings;
import be.manhart.graphtrans.chart.option.XaxisSettings;
import be.manhart.graphtrans.chart.option.XaxisSettings.Mode;

/**
 * Das Timeline Chart nimmt ein {@link IModel}<{@link LineChartData}>.
 * 
 * @see AbstractLineChart
 * @author Manuel
 */
public class TimelineChart extends AbstractLineChart<TimelineChartData, Date, Float> {
	private static final long serialVersionUID = 1L;

	public TimelineChart() {
		super();
	}

	public TimelineChart(final OptionSettings... options) {
		super(options);
	}

	@Override
	protected OptionSettings[] getDefaultOptions() {
		final List<OptionSettings> options = new ArrayList<OptionSettings>(Arrays.asList(super.getDefaultOptions()));
		options.add(new XaxisSettings().setMode(Mode.TIME).setAutoscale(true));
		return options.toArray(new OptionSettings[options.size()]);
	}

	@Override
	protected String convertX(final Date xAxisDate) {
		XaxisSettings xAxisSetting = findSetting(XaxisSettings.class);
		if (xAxisSetting == null) {
			xAxisSetting = new XaxisSettings();
			addSettings(xAxisSetting);
		}
		String dateString = null;
		dateString = DateUtils.getFormattedDate(xAxisDate, "yyyy/MM/dd HH:mm:ss");
//		System.out.println("date2format: " + xAxisDate + " => " + dateString);
		// TimeUnit timeUnit = xAxisSetting.getTimeUnit();
		// switch (timeUnit) {
		//
		// case MILLISECOND:
		// return DateUtils.getFormattedDate(xAxisDate, "ssSSSS");
		// case SECOND:
		// return DateUtils.getFormattedDate(xAxisDate, "ss");
		// case MINUTE:
		// return DateUtils.getFormattedDate(xAxisDate, "hhmm");
		// case HOUR:
		// dateString = DateUtils.getFormattedDate(xAxisDate, "MM/dd/yyyy
		// hh:mi:ss.SSSS");
		// break;
		// case DAY:
		// case MONTH:
		// case YEAR:
		// // is default format
		// default:
		// xAxisSetting.setTimeUnit(TimeUnit.YEAR);
		// break;
		// }
		if (dateString != null) {
			return "new Date(\"" + dateString + "\").getTime()";
		}
		return null;
	}

	@Override
	protected String convertY(final Float y) {
		return y.toString();
	}

}
