package be.manhart.graphtrans.chart;

import java.io.Serializable;
import java.util.List;

public interface Line<T extends Coordinates<?, ?>> extends Serializable {

	public List<T> getPoints();

	public void setPoints(List<T> points);

	public boolean isAlternateYaxis();

	public String getColor();

	public void setColor(String color);

	public String getLabel();

	void setLabel(String label);
}