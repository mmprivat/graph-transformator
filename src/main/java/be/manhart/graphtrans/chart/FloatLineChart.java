package be.manhart.graphtrans.chart;

import be.manhart.graphtrans.chart.option.OptionSettings;

/**
 * Das Line Chart nimmt ein {@link IModel}<{@link LineChartData}>.
 * 
 * @see BaseChart
 * @author Manuel
 */
public class FloatLineChart extends 
	AbstractLineChart<FloatLineChartData, Float, Float> {
	private static final long serialVersionUID = 1L;

	public FloatLineChart() {
		super();
	}

	public FloatLineChart(OptionSettings... options) {
		super(options);
	}

	protected String convertX(Float x) {
		return x != null ? x.toString() : "";
	}

	protected String convertY(Float y) {
		return y != null ? y.toString() : "";
	}
}
