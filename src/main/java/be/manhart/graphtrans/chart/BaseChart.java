package be.manhart.graphtrans.chart;

import java.io.Serializable;

import be.manhart.graphtrans.chart.option.OptionSettings;
import be.manhart.graphtrans.chart.option.RootSettings;

/**
 * Um dieses Chart zu verwenden, übergibt man ihm ein {@link IModel} auf das
 * entsprechende Objekt. <br/>
 * Um z.B. Filter einzubauen, braucht man nur die {@link IModel#getObject()}
 * Methode zu überschreiben und entsprechend gefilterte Daten zurück geben, die
 * Aktualisierung geschieht dann automatisch.
 * 
 * @author Manuel
 * 
 * @param <T>
 */
public abstract class BaseChart<T extends Serializable> implements Serializable {
	private static final long serialVersionUID = 1L;

	private RootSettings rootSettings;

	public BaseChart() {
		// initialize settings
		clearSettings();
		addSettings(getDefaultOptions());
	}

	public BaseChart(final OptionSettings... options) {
		clearSettings();
		if (options != null && options.length > 0) {
			addSettings(options);
		}
	}

	protected abstract OptionSettings[] getDefaultOptions();

	/**
	 * This method creates a {@link Model} which prints the Flotr2 js chart
	 * function into a script-Tag.<br/>
	 * Therefor it calls
	 * {@link BaseChart#convertDataToString(Serializable)} and
	 * {@link BaseChart#getOptionsString()}.
	 * 
	 * @see <a href="http://www.humblesoftware.com/flotr2/documentation">flotr2
	 *      Documentation</a>
	 * @param chartContainerId
	 * @return
	 */
	public String toString(final String chartContainerId, final T data) {
		return "Flotr.draw(document.getElementById('" + chartContainerId + "'), " + convertDataToString(data) + ", "
				+ getOptionsString() + ");";
	}

	/**
	 * Converts the chart data into a js array, which is put into the Flotr2
	 * chart. This method is called from
	 * {@link BaseChart#getChart(String)}.
	 * 
	 * @see BaseChart#getChart(String)
	 * @param object
	 * @return
	 */
	protected abstract String convertDataToString(T object);

	/**
	 * This method converts all {@link ChartOption}s into a {@link String} for
	 * further convinient useage.<br/>
	 * 
	 * @return all options in flotr2 format or null
	 */
	private String getOptionsString() {
		final String flotrOptions = rootSettings.toFlotrString();
		final StringBuilder sb = new StringBuilder();
		if (flotrOptions != null && flotrOptions.length() > 0) {
			sb.append("{");
			sb.append(flotrOptions);
			// remove last ','
			if (sb.charAt(sb.length() - 1) == '\'') {
				sb.replace(sb.length() - 2, sb.length() - 1, "");
			}
			sb.append("}");
		}
		return sb.toString();
	}

	public BaseChart<T> clearSettings() {
		rootSettings = new RootSettings();
		return this;
	}

	public <E extends OptionSettings> E findSetting(final Class<E> settings) {
		if (settings != null) {
			return rootSettings.findSetting(settings);
		}
		return null;
	}

	public BaseChart<T> addSettings(final OptionSettings... settings) {
		if (settings != null && settings.length > 0) {
			for (final OptionSettings item : settings) {
				rootSettings.mergeSettings(item);
			}
		}
		return this;
	}
}
