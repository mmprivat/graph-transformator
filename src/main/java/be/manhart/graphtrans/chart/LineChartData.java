package be.manhart.graphtrans.chart;

import java.io.Serializable;
import java.util.List;

public interface LineChartData<T extends Line<?>> extends Serializable {

	public List<T> getLines();

	public void setLines(List<T> lines);
}