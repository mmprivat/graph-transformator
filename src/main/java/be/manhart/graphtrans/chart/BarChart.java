package be.manhart.graphtrans.chart;

import be.manhart.graphtrans.chart.option.BarChartSettings;
import be.manhart.graphtrans.chart.option.GridSettings;
import be.manhart.graphtrans.chart.option.OptionSettings;
import be.manhart.graphtrans.chart.option.XaxisSettings;
import be.manhart.graphtrans.chart.option.YaxisSettings;

/**
 * Das Bar Chart nimmt ein {@link IModel}<{@link LineChartData}>.
 * 
 * @see AbstractLineChart
 * @author Manuel
 */
public class BarChart extends BaseChart<BarChartData> {
	private static final long serialVersionUID = 1L;

	public BarChart() {
		super();
	}

	public BarChart(OptionSettings... options) {
		super(options);
	}

	@Override
	protected OptionSettings[] getDefaultOptions() {
		return new OptionSettings[] { new BarChartSettings(),
				new XaxisSettings().setAutoscale(true).setShowLabels(true),
				new YaxisSettings().setShowLabels(true),
				new GridSettings().setMinorVerticalLines(false) };
	}

	@Override
	protected String convertDataToString(BarChartData filteredData) {
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		if (filteredData.getBarSequence().length > 0
				&& filteredData.getBarSequence()[0].getBars().length > 0) {
			// add all lines
			int lineCount = 0;
			for (BarSequence barSequence : filteredData.getBarSequence()) {
				// add start
				if (lineCount > 0) {
					sb.append(",");
				}
				lineCount++;
				sb.append("{");
				// add all points
				sb.append("data: [");
				for (Bar bar : barSequence.getBars()) {
					sb.append("[" + bar.getX() + "," + bar.getY() + "],");
				}
				sb.append("],");
				// add label
				sb.append("label: '");
				sb.append(barSequence.getLabel());
				sb.append("'");
				sb.append("}");
			}
		}
		sb.append("]");
		return sb.toString();
	}
}
