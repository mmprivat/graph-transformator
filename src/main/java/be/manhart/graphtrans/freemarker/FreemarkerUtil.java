package be.manhart.graphtrans.freemarker;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Map;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;

public class FreemarkerUtil {

	public static String writeFreemarkerFileFromTemplate(String templatePath, String templateName, Map<String, ?> context)
			throws TemplateException, IOException {
		// Create your Configuration instance, and specify if up to what
		// FreeMarker
		// version (here 2.3.25) do you want to apply the fixes that are not
		// 100%
		// backward-compatible. See the Configuration JavaDoc for details.
		final Configuration cfg = new Configuration();

		// Specify the source where the template files come from.
		if (templatePath != null) {
			cfg.setDirectoryForTemplateLoading(new File(templatePath));
		} else {
			cfg.setClassForTemplateLoading(FreemarkerUtil.class, "");
		}

		// Set the preferred charset template files are stored in. UTF-8 is
		// a good choice in most applications:
		cfg.setDefaultEncoding("UTF-8");

		// Sets how errors will appear.
		cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);

		final Template temp = cfg.getTemplate(templateName);
		final Writer out = new StringWriter();
		temp.process(context, out);
		return out.toString();
	}
}
