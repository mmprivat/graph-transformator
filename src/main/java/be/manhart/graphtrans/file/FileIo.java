package be.manhart.graphtrans.file;

import java.io.IOException;
import java.net.URI;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class FileIo {

	public static List<String> readFile(final String filePath) {
		if (filePath == null) {
			throw new IllegalArgumentException("File Path is null");
		}
		return readFile(Paths.get(filePath));
	}

	public static List<String> readFile(final URI filePath) {
		if (filePath == null) {
			throw new IllegalArgumentException("File Path is null");
		}
		return readFile(Paths.get(filePath));
	}

	public static List<String> readFile(final Path filePath) {
		// read file into stream, try-with-resources
		List<String> result = new ArrayList<>();
		if (!filePath.toFile().exists()) {
			throw new IllegalArgumentException("File '" + filePath.toFile().getAbsolutePath() + "' does not exist.");
		}
		try (Stream<String> stream = Files.lines(filePath)) {
			stream.forEach(c -> result.add(c));
		} catch (final IOException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public static void writeFile(final String filePath, String... content) throws IOException {
		writeFile(Paths.get(filePath), content);
	}
	
	public static void writeFile(final String filePath, List<String> content) throws IOException {
		writeFile(Paths.get(filePath), content);
	}

	public static void writeFile(final URI filePath, List<String> content) throws IOException {
		writeFile(Paths.get(filePath), content);
	}

	public static void writeFile(final Path filePath, List<String> content) throws IOException {
		Files.write(filePath, content, Charset.forName("UTF-8"));
	}

	public static void writeFile(Path filePath, String... content) throws IOException {
		writeFile(filePath, Arrays.asList(content));
	}

}
