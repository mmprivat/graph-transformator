package be.manhart.graphtrans;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import be.manhart.graphtrans.chart.TimeCoordinates;
import be.manhart.graphtrans.chart.Timeline;
import be.manhart.graphtrans.chart.TimelineChart;
import be.manhart.graphtrans.chart.TimelineChartData;
import be.manhart.graphtrans.chart.option.GridSettings;
import be.manhart.graphtrans.chart.option.LegendSettings;
import be.manhart.graphtrans.chart.option.LineChartSettings;
import be.manhart.graphtrans.chart.option.MarkerSettings;
import be.manhart.graphtrans.chart.option.MouseSettings;
import be.manhart.graphtrans.chart.option.OptionSettings;
import be.manhart.graphtrans.chart.option.PointsSettings;
import be.manhart.graphtrans.chart.option.XaxisSettings;
import be.manhart.graphtrans.chart.option.YaxisSettings;
import be.manhart.graphtrans.file.FileIo;
import be.manhart.graphtrans.freemarker.FreemarkerUtil;
import freemarker.template.TemplateException;

public class GraphTransformator {
	private static final String FLOTR2_JS = "flotr2.min.js";
	private static final String CHARTS_HELPERS_JS = "charts-helpers.js";

	protected static final String SEPARATOR = ";";

	protected static final Pattern LOG_ENTRY_DATE = Pattern.compile("Date: ([0-9]+)");
	protected static final Pattern LOG_ENTRY_UPLOAD = Pattern.compile(" +Upload: +([0-9.]+) Mb");
	protected static final Pattern LOG_ENTRY_DOWNLOAD = Pattern.compile(" +Download: +([0-9.]+) Mb");
//	protected static final Pattern LOG_ENTRY_PING = Pattern.compile("Hosted by [A-Za-z0-9() ]+ \\[([0-9.]+) km\\]: ([0-9.]+) ms");
	protected static final Pattern LOG_ENTRY_PING = Pattern.compile(" +Latency: +([0-9.]+) ms");
	protected static final String LOG_ENTRY_END = "--- TEST FINISHED ---";
	protected static final String LOG_ENTRY_START = "--- TEST STARTED ---";

	// OLD FORMAT
	// --- TEST STARTED ---
	// Date: 1477345945
	// Date (formatted): Mon Oct 24 23:52:25 CEST 2016
	// Retrieving speedtest.net configuration...
	// Retrieving speedtest.net server list...
	// Testing from 3 (178.165.128.225)...
	// Selecting best server based on latency...
	// Hosted by Nessus GmbH (10G Uplink) (Vienna) [1.07 km]: 37.54 ms
	// Testing download speed........................................
	// Download: 18.68 Mbit/s
	// Testing upload speed..................................................
	// Upload: 11.15 Mbit/s
	// --- TEST FINISHED ---
	// NEW FORMAT
	//	--- TEST STARTED ---
	//	Date: 1711262333
	//	Date (formatted): So 24 Mär 2024 07:38:53 CET
	//
	//	   Speedtest by Ookla
	//
	//	     Server: BlackHOST Ltd. - Vienna (id = 46058)
	//	        ISP: Telekom Austria
	//	    Latency:     9.76 ms   (2.94 ms jitter)
	//
	//	   Download:   128.59 Mbps (data used: 77.2 MB )                               
	//
	//	     Upload:    16.65 Mbps (data used: 24.8 MB )                               
	//	Packet Loss:     0.0%
	//	 Result URL: https://www.speedtest.net/result/c/1444c9d1-dd94-4c34-98aa-49f88971becb
	//	--- TEST FINISHED ---
	private Timeline pingChartData;
	private Timeline uploadChartData;
	private Timeline downloadChartData;
	private Config config;

	public GraphTransformator(Config config) {
		this.config = config;
	}

	public void loadFile() {
		initChartData();
		List<String> content = FileIo.readFile(config.getInputFile());

		Date date = new Date();
		boolean logEntryStarted = false;
		for (String line : content) {
			if (logEntryStarted) {
				setIfMatches(LOG_ENTRY_DATE, line, m -> new Date(Long.parseLong(m) * 1000),
						c -> date.setTime(c.getTime()));
				setIfMatches(LOG_ENTRY_PING, line, m -> Float.parseFloat(m),
						c -> pingChartData.getPoints().add(new TimeCoordinates(new Date(date.getTime()), c)));
//				setIfMatches(LOG_ENTRY_PING, line, m -> Float.parseFloat(m.substring(m.lastIndexOf(SEPARATOR))),
//						pingChartData, new Date(date.getTime()));
				// setIfMatches(LOG_ENTRY_DOWNLOAD, line, m ->
				// Float.parseFloat(m),
				// downloadChartData, new Date(date.getTime()));
				setIfMatches(LOG_ENTRY_DOWNLOAD, line, m -> Float.parseFloat(m),
						c -> downloadChartData.getPoints().add(new TimeCoordinates(new Date(date.getTime()), c)));
				setIfMatches(LOG_ENTRY_UPLOAD, line, m -> Float.parseFloat(m),
						c -> uploadChartData.getPoints().add(new TimeCoordinates(new Date(date.getTime()), c)));

				if (LOG_ENTRY_END.equals(line)) {
					logEntryStarted = false;
				}
			} else if (LOG_ENTRY_START.equals(line)) {
				logEntryStarted = true;
			} else {
				// ignore line(s)
			}
		}
	}

	private <T> void setIfMatches(Pattern pattern, String line, Function<String, T> mapper, Consumer<T> setter) {
		Matcher m = pattern.matcher(line);
		if (m.find()) {
			T t = null;
			if (m.groupCount() == 1) {
				// System.out.println("Input String: " + m.group(1));
				t = mapper.apply(m.group(1));
				// System.out.println("mapped Object: " + t);
			} else if (m.groupCount() > 1) {
				StringBuilder sb = new StringBuilder();
				for (int i = 1; i <= m.groupCount(); i++) {
					sb.append(m.group(i)).append(SEPARATOR);
				}
				t = mapper.apply(sb.substring(0, sb.length() - 1));
			}
			setter.accept(t);
		}
	}

	private void initChartData() {
		pingChartData = new Timeline("Ping (ms)", new ArrayList<>());
		downloadChartData = new Timeline("Download (Mbit/s)", new ArrayList<>());
		uploadChartData = new Timeline("Upload (Mbit/s)", new ArrayList<>());
	}

	public void writeFile() throws TemplateException, IOException {
		Map<String, String> context = new HashMap<>();

		OptionSettings[] options = { new LegendSettings().setPosition("ne").setBackgroundColor("#aaddff"),
				new PointsSettings(), new LineChartSettings(), 
				new XaxisSettings().setAutoscale(true).setNoTicks(10).setTickFormatter("function(obj) {return getFormattedDateTime(obj);}")
				.setLabelsAngle(90),
				new YaxisSettings().setAutoscale(true).setShowLabels(true).setMin(0l),
				new GridSettings().setVerticalLines(true).setHorizontalLines(true),
				new MarkerSettings().setPosition("tc"),
				new MouseSettings().setTrack(true).setTrackFormatter("function(obj) { return obj.series.label + ': ' + getFormattedDateTime(obj.x) + ' - ' + obj.y; }")
		};
		Collections.sort(uploadChartData.getPoints(), new TimelineComparator());
		Collections.sort(downloadChartData.getPoints(), new TimelineComparator());
		String ping = new TimelineChart(options).toString("ping-chart", new TimelineChartData(pingChartData));
		String upload = new TimelineChart(options).toString("upload-chart", new TimelineChartData(uploadChartData));
		String download = new TimelineChart(options).toString("download-chart", new TimelineChartData(downloadChartData));

		context.put("ping", ping);
		context.put("download", download);
		context.put("upload", upload);

		String tempFilePath = null;
		String tempFileName = "speedtest.ftl";
		if (config.getTemplateFile() != null) {
			File _file = new File(config.getTemplateFile());
			tempFilePath = new File(_file.getParent()).getAbsolutePath();
			tempFileName = _file.getName();
		}
		String content = FreemarkerUtil.writeFreemarkerFileFromTemplate(tempFilePath, tempFileName, context);

		File outputFile = new File(config.getOutputFile());
		String outputFilePath;
		if (outputFile.getParent() != null) {
			outputFilePath = new File(outputFile.getParent()).getAbsolutePath();
		} else {
			outputFilePath = new File( "." ).getAbsolutePath();
		}

		FileIo.writeFile(Paths.get(outputFilePath, outputFile.getName()), content);
		FileIo.writeFile(Paths.get(outputFilePath, FLOTR2_JS), getFileContent(tempFilePath, FLOTR2_JS));
		FileIo.writeFile(Paths.get(outputFilePath, CHARTS_HELPERS_JS),
				getFileContent(tempFilePath, CHARTS_HELPERS_JS));
	}

	private List<String> getFileContent(String filePath, String fileName) {
		if (filePath != null) {
			return FileIo.readFile(Paths.get(filePath, fileName));
		} else {
			return readInputStream(FreemarkerUtil.class.getResourceAsStream(fileName));
		}
	}
	
	private static List<String> readInputStream(InputStream is) {
		BufferedReader br = null;
		List<String> result = new ArrayList<>();

		String line;
		try {

			br = new BufferedReader(new InputStreamReader(is));
			while ((line = br.readLine()) != null) {
				result.add(line);
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return result;

	}
}
