package be.manhart.graphtrans;

import java.util.Comparator;

import be.manhart.graphtrans.chart.TimeCoordinates;

public class TimelineComparator implements Comparator<TimeCoordinates> {

	@Override
	public int compare(TimeCoordinates o1, TimeCoordinates o2) {
		if (o1 != null && o2 != null) {
			return o1.getX().compareTo(o2.getX());
		}
		return 0;
	}

}
